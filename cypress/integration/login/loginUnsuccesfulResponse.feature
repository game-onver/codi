Feature: Unsuccessful login at Codibly’s QA Automation Recruitment Task

  Scenario Outline: 01 - Unsuccessful login -  response 404

    Given Open login page
    And Fill email field = <email>
    And Fill password field = <pass>
    And RESTfull Request failed
    When Response unsuccessful checkbox marked
    And Request failed

    Examples:
      | email           | pass       |
      | email@email.com | Password1  |
