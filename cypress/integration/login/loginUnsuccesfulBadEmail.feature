Feature: Unsuccessful login at Codibly’s QA Automation Recruitment Task

  Scenario Outline: Unsuccessful login bad email format

    Given Open login page
    And Fill email field = <email>
    And Fill password field = <pass>
    When Press login button
    Then Please provide correct email address

    Examples:
      | email          | pass       |
      | emailemail.com | password1  |
      | Email@email    | Password1  |
      | 1232434        | password_1 |