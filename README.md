![node](https://img.shields.io/badge/node-%3E%3D%2012.18.2-brightgreen.svg) [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff1111.svg)](https://github.com/prettier/prettier)
[![Cypress.io](https://img.shields.io/badge/tested%20with-Cypress-ff99999.svg)](https://www.cypress.io/)
[![This project is using Percy.io for visual regression testing.](https://percy.io/static/images/percy-badge.svg)](https://percy.io/ad4be792/cypress-vs-paca)
## cypress.io + tc in gherkin/cucumber

Codibly’s QA Automation Recruitment Task

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Technical

The project uses a number of open source projects to work properly:

* [Node.js] - Development platform for executing JavaScript code server-side
* [npm] - Package manager for JavaScript
* [Cypress.io] - Modern testing tool with cross-browser compatibility
* [Percy] - Visual regression tool
* [cypress-cucumber-preprocessor] - Run cucumber/gherkin-syntaxed specs with Cypress.io

[npm]: <https://www.npmjs.com/>
[Node.js]: <http://nodejs.org>
[Cypress.io]: <http://cypress.io>
[Percy]: <https://percy.io/>
[cypress-cucumber-preprocessor]:<https://github.com/TheBrainFamily/cypress-cucumber-preprocessor>

### Prerequisites

Cypress.io requires [Node](https://nodejs.org/) 12.18.2+ to run.

### Installation

```sh
install node.js ver 12 or 14 and above
and chrome ver 91+ 
git clone https://github.com/game-onver/codi
```

Make sure that you are up to date:

```sh
$ node -v
$ npm -v
```

Install the dependencies and devDependencies:

```sh
$ npm i   (current cypress version is 7.4.0)
$ npx ncu -u   (update you dependecied to latest version)  
$ npm audit fix --registry https://registry.npmjs.org/  (fix vulnerabilites)
```

Run cypress test runner:

```sh
$ npm run cy:open
```

for headless mode

```sh
$ npm run cy:run
```
___

   [![Node.js|](https://cdn.iconscout.com/icon/free/png-128/nodejs-4-458159.png)](https://nodejs.org/en/)