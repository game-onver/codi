const cucumber = require('cypress-cucumber-preprocessor').default
//const percyHealthCheck = require('@percy/cypress/task')

module.exports = on => {
    //on('task', percyHealthCheck)
    on('file:preprocessor', cucumber())
}
