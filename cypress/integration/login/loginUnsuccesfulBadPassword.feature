Feature: Unsuccessful login at Codibly’s QA Automation Recruitment Task

  Scenario Outline: Unsuccessful login bad password format - Password should contain a number

    Given Open login page
    And Fill email field = <email>
    And Fill password field = <pass>
    When Press login button
    Then Password should contain a number

    Examples:
      | email           | pass        |
      | email@email.com | passworddd  |
      | email@email.com | Passworddd  |
      | email@email.com | passworddd_ |

  Scenario Outline: Unsuccessful login bad password format - Password should contain at least 10 characters

    Given Open login page
    And Fill email field = <email>
    And Fill password field = <pass>
    When Press login button
    Then Password should contain at least 10 characters
#3 case contains bug (login is successful at 9 characters)
    Examples:
      | email           | pass      |
      | email@email.com | 1         |
      | email@email.com | Abcdefg1  |
      | email@email.com | Aa2345678 |

  Scenario Outline: Unsuccessful login bad password format - Password should contain an uppercase letter

    Given Open login page
    And Fill email field = <email>
    And Fill password field = <pass>
    When Press login button
    Then Password should contain an uppercase letter
#1 case contains bug (login is successful without Uppercase letter)
    Examples:
      | email           | pass         |
      | email@email.com | 1abcdefghijk |
      | email@email.com | 1234567890   |