Feature: Successful login at Codibly’s QA Automation Recruitment Task

  Scenario Outline: 01 - Successful login

    Given Open login page
    And Assert login page view
    And Fill email field = <email>
    And Fill password field = <pass>
    When Press login button
    Then Assert that user is logged in

    Examples:
      | email           | pass       |
      | email@email.com | password1  |
      | Email@email.com | Password1  |
      | email@email.Com | password_1 |