import { Given, And, Then, When } from "cypress-cucumber-preprocessor/steps";

Given("Open login page", () => {
  cy.visit("");
  cy.get("title").invoke("text").should("equals", "Codibly - QA Automation Recruitment Task");
});

And("Assert login page view", () => {
  cy.get(".sc-AxmLO").should("exist");
  cy.get(".sc-fzozJi").should("exist");
  cy.get("[data-testid=app__login-section]").should("be.visible");
  cy.contains("Hey Buddy!").should("exist").should("be.visible");
  cy.contains("Welcome to Codibly’s QA Automation Recruitment Task").should("exist").should("be.visible");
  cy.get(":nth-child(1) > .MuiInputBase-root > .MuiInputBase-input").should("be.visible");
  cy.get(":nth-child(2) > .MuiInputBase-root > .MuiInputBase-input").should("be.visible");
  cy.get(".PrivateSwitchBase-input-8").should("have.value", "false");
  cy.contains("Should response be unsuccessfull?").should("be.visible");
  cy.get(".MuiButton-root").should("be.visible");
  cy.get(".MuiButton-label").should("be.visible");
  cy.get(".sc-fzpans").should("be.visible");
});

And("Fill email field = {}", email => {
  cy.get(":nth-child(1) > .MuiInputBase-root > .MuiInputBase-input").click().type(email);
});

And("Fill password field = {}", pass => {
  cy.get(":nth-child(2) > .MuiInputBase-root > .MuiInputBase-input").click().type(pass);
});

And("Press login button", () => {
  cy.get(".MuiButton-label").click();
});

And("Assert that user is logged in", () => {
  cy.get("div.sc-Axmtr.ipDUek").should("be.visible");
  cy.get(".sc-fzpans").should("be.visible");
});

And("Please provide correct email address", () => {
  cy.get(".MuiFormHelperText-root").should("have.text", "Please provide correct email address");
  cy.contains("Successfuly logged in!", { timeout: 100 }).should("not.exist");
});

And("Password should contain a number", () => {
  cy.get(".MuiFormHelperText-root").should("have.text", "Password should contain a number");
  cy.contains("Successfuly logged in!", { timeout: 100 }).should("not.exist");
});

And("Password should contain at least 10 characters", () => {
  cy.get(".MuiFormHelperText-root").should("have.text", "Password should contain at least 10 characters");
  cy.contains("Successfuly logged in!", { timeout: 100 }).should("not.exist");
});

And("Password should contain an uppercase letter", () => {
  cy.get(".MuiFormHelperText-root").should("have.text", "Password should contain an uppercase letter");
  cy.contains("Successfuly logged in!", { timeout: 100 }).should("not.exist");
});

And("Response unsuccessful checkbox marked", () => {


  cy.get(".PrivateSwitchBase-input-8").should("have.value", "false");
  cy.get(".PrivateSwitchBase-input-8").click();
  cy.get(".PrivateSwitchBase-input-8").should("have.value", "true");
  cy.get(".MuiButton-label").click();

});

And("Request failed", () => {
  cy.get('[data-testid=login-form__form-error]').should("be.visible").should('contain.text','Login failed: invalid username or password')
});

And("RESTfull Request failed", () => {
  cy.server();
  cy.request({
    method: "POST",
    url: "https://run.mocky.io/v3/5cc28ca6-1e6c-4a34-91a8-2c0d7f24e299",
    failOnStatusCode: false,
    body: {
      email: "email@email.pl",
      password: "Abc123456789"
    }}
  ).then(response => {
    expect(response).property("status").to.equal(401);
    expect(response).property("body").to.exist;
    expect(response.body).property('errorMessage').to.be.a("string");
    expect(response.body).property('errorMessage').to.eql('Login failed: invalid username or password');

  });
});

